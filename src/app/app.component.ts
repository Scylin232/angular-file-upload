import { Component } from '@angular/core';
import { HttpClient, HttpEventType } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  progressState: string;
  selectedFile = null;

  constructor(private http: HttpClient) {}

  onFileSelected(event: any) {
    this.selectedFile = event.target.files[0];
  }

  onUpload() {
    const fd = new FormData();
    fd.append('file', this.selectedFile, this.selectedFile.name);
    this.http.post('http://localhost:4200/', fd, { // localhost - Any Backend, who Accept Form-File Data.
      reportProgress: true,
      observe: 'events'
    })
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.progressState = 'Upload Progress: ' + Math.round(event.loaded / event.total * 100) + '%';
        } else if (event.type === HttpEventType.Response) {
          this.progressState = 'Success!';
          console.log(event);
        }
      });
  }
}
